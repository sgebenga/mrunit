package mrunit;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class SMSCDRWithCounterMapperTest {

    private static final String CDR = "1234;0;0761234567;0821234567;6";

    MapDriver<LongWritable, Text, Text, IntWritable> mapDriver = MapDriver.newMapDriver(new SMSCDRWithCounterMapper());

    @Test
    public void testMapper() throws IOException {
        mapDriver.withInput(new LongWritable(), new Text(CDR));
        mapDriver.runTest();
        assertEquals(1, mapDriver.getCounters().findCounter(SMSCDRWithCounterMapper.CDRCounter.NONSMSCDR).getValue());
    }

}
