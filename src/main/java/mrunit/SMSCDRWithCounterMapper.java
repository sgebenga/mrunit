package mrunit;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class SMSCDRWithCounterMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    private Text status = new Text();
    private final static IntWritable ONE = new IntWritable();
    private final static String DELIMITER = ";";

    static enum CDRCounter {
        NONSMSCDR;
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] line = value.toString().split(DELIMITER);
        if (Integer.parseInt(line[1]) == 1) {
            status.set(line[4]);
            context.write(status, ONE);
        } else {
            context.getCounter(CDRCounter.NONSMSCDR).increment(1);
        }

    }
}
