package mrunit;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class SMSCDRMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    private Text status = new Text();
    private static final IntWritable ONE = new IntWritable(1);
    private static final String SEMI_COLON = ";";

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String[] line = value.toString().split(SEMI_COLON);
        if (Integer.parseInt(line[1]) == 1) {
            status.set(line[4]);
            context.write(status, ONE);
        }
    }
}
